package main

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

// User is a user.
type User struct {
	Mdn      string `json:"mdn"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// JwtToken is a JWT token.
type JwtToken struct {
	Token string `json:"token"`
}

// Exception is an exception.
type Exception struct {
	Message string `json:"message"`
}

// Response is a response.
type Response struct {
	Data string `json:"data"`
}

// Basic auth secret
const USERNAME = "batman"
const PASSWORD = "secret"

// JwtKey is the key used to sign JWT tokens.
var JwtKey = []byte(os.Getenv("JWT_KEY"))

// Users is a list of users.
var Users = []User{
	User{
		Username: "user1",
		Password: "password1",
	},
	User{
		Username: "user2",
		Password: "password2",
	},
}

// CreateToken creates a JWT token.
func CreateToken(w http.ResponseWriter, r *http.Request) {
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		"password": user.Password,
		"exp":      time.Now().Add(time.Hour * time.Duration(1)).Unix(),
	})
	tokenString, error := token.SignedString(JwtKey)
	if error != nil {
		fmt.Println(error)
	}
	json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
}

// ValidateMiddleware validates the JWT token.
func ValidateMiddlewareJWT(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return JwtKey, nil
				})
				if error != nil {
					json.NewEncoder(w).Encode(Exception{Message: error.Error()})
					return
				}
				if token.Valid {
					next.ServeHTTP(w, r)
				} else {
					json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
				}
			}
		} else {
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}

func ValidateMiddlewareBasic(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()

		if !ok {
			w.Write([]byte(`something went wrong`))
			json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
		}

		isValid := (username == USERNAME) && (password == PASSWORD)

		if isValid == false {
			json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})

		}

		next.ServeHTTP(w, r)

	})
}

// ProtectedEndpoint is a protected endpoint.
func ProtectedEndpointJWT(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	mdn := params["mdn"][0]
	authorizationHeader := r.Header.Get("authorization")
	bearerToken := strings.Split(authorizationHeader, " ")

	w.Header().Add("Content-Type", "application/json")

	token, _ := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return JwtKey, nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var user User
		user.Mdn = mdn
		mapstructure.Decode(claims, &user)
		json.NewEncoder(w).Encode(user)
	} else {
		json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
	}

}

func ProtectedEndpointBasic(w http.ResponseWriter, r *http.Request) {

	params := r.URL.Query()
	mdn := params["mdn"][0]

	u, p, ok := r.BasicAuth()
	if !ok {
		fmt.Println("Error parsing basic auth")
		w.WriteHeader(401)
		return
	}
	if u != USERNAME {
		fmt.Printf("Username provided is correct: %s\n", u)
		w.WriteHeader(401)
		return
	}
	if p != PASSWORD {
		fmt.Printf("Password provided is correct: %s\n", u)
		w.WriteHeader(401)
		return
	}

	user := User{
		Mdn:      mdn,
		Username: u,
		Password: p,
	}

	json.NewEncoder(w).Encode(user)

}

func main() {
	router := mux.NewRouter()
	fmt.Println("Starting the application...")
	router.HandleFunc("/authenticate", CreateToken).Methods("POST")
	router.HandleFunc("/jwtToken", ValidateMiddlewareJWT(ProtectedEndpointJWT)).Methods("GET")
	router.HandleFunc("/basicAuth", ValidateMiddlewareBasic(ProtectedEndpointBasic)).Methods("GET")
	log.Fatal(http.ListenAndServe(":9090", router))
}
